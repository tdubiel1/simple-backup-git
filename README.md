# simple-backup
For this demo to work in your own environment you can fork it so you can change the files. Also you would need to create a separate gitlab repo/project call remote_backup to store your router configs. If you prefer a single repo, then simply modify the variables in vars/git.yml and vars/vault.yml to go to the same repo.


## Getting started in Ansible-Navigator or AAP Controller

*Create a gitlab token for your project "remote_backup"
    * Settings - Access Tokens
*Create a vault.yml file with the following credentials for 

~~~
---
git_username: git config --global user.name <your_username>
git_remote: git remote set-url origin https://<your_username>:<your_token>@gitlab.com/<your_username>/router_backups.git/  
~~~
# AAP Controller
In AAP Controller you will need a vault credential with the vault password to decrypt vars/vault.yml. This credential will be applied to the job-template. 

# Ansible-Navigator
Run the playbook like this:
~~~
 ansible-navigator run backup_git.yml -m stdout --vault-password-file ~/.rhv/vault-git
~~~
Change the path to your vault password file

# Work in progress
In the near future I'll add a controller.yml playbook to automate the AAP controller as code configs for the job-template, etc.